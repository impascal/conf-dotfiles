#!/bin/bash

# Escape Seq. for Colors
txtblk='\[\033[0;30m\]' # Black - Regular
txtred='\[\033[0;31m\]' # Red
txtgrn='\[\033[0;32m\]' # Green
txtylw='\[\033[0;33m\]' # Yellow
txtblu='\[\033[0;34m\]' # Blue
txtpur='\[\033[0;35m\]' # Purple
txtcyn='\[\033[0;36m\]' # Cyan
txtwht='\[\033[0;37m\]' # White
bldblk='\[\033[1;30m\]' # Black - Bold
bldred='\[\033[1;31m\]' # Red
bldgrn='\[\033[1;32m\]' # Green
bldylw='\[\033[1;33m\]' # Yellow
bldblu='\[\033[1;34m\]' # Blue
bldpur='\[\033[1;35m\]' # Purple
bldcyn='\[\033[1;36m\]' # Cyan
bldwht='\[\033[1;37m\]' # White
txtrst='\[\033[0m\]'    # Text Reset

# Autocd
shopt -s autocd

# Cdspell
shopt -s cdspell

# Vi-mode
set -o emacs

# History
HISTSIZE=1000
HISTFILESIZE=$HISTSIZE
HISTCONTROL=ignoredups
export HISTIGNORE="clear*:"

# Gitstatus prompt
git_prompt() {
	[ -d ./.git ] && git branch | grep -G '*' | awk '{print $2}'
}

# Prompt
PS1="$bldred[$bldylw\w$bldred] $txtrst> "

# cd on quit nnn
nn() {
	nnn "$@"
	if [ -f $NNN_TMPFILE ]; then
		. $NNN_TMPFILE
		rm -f $NNN_TMPFILE > /dev/null
	fi
}

# System aliases
alias ss='sudo systemctl'
alias pac='sudo pacman'
alias mkicpio='sudo mkinitcpio -p linux'

# Configs files
alias i3conf='nvim ~/.config/i3/config'
alias polyconf='nvim ~/.config/polybar/config'
alias nvimconf='nvim ~/.config/nvim/init.vim'
alias compconf='nvim ~/.config/compton/compton.conf'
alias dunstconf='nvim ~/.config/dunst/dunstrc'
alias cleanreposuckless='make clean && rm -f config.h && git reset --hard origin/master'

# Programs
alias nv='nvim'
alias vim='nvim'
alias n='nn -l'
alias na='nn -ld'

# Routine aliases
alias ls='ls --color=auto'
alias la='ls -a --color=auto'
alias cp='cp -rv'
alias mv='mv -v'
alias mkdir='mkdir -v'
alias rm='rm -rv'
alias rmexe='find . -maxdepth 1 -type f -executable -delete'
alias untar='tar -xvf'
alias fetch='neofetch --off'
alias nowhitelines="sed '/^\s*$/d'"
