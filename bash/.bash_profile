# Paths
export PATH="$PATH:$HOME/scripts"
export HOME="/home/matteo"
export XDG_CONFIG_HOME="$HOME/.config"

# Default programs
export OPEN="simple-open"
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="firefox"
export PDFREADER="zathura"
export DOCOPENER="libreoffice --writer"
export PPTOPENER="libreoffice --impress"
export IMGVIEWER="feh"
export VIDPLAYER="mpv"
export MYDMENU="dmenu -i -fn Inconsolata-11 -nb #161819 -nf #666666 -sb #161819 -sf #ebdbb2"
export SUDO_ASKPASS="$HOME/scripts/dmenu-pass"

# NNN settings
export NNN_USE_EDITOR=1
export NNN_OPENER="$HOME/scripts/sl-opener"
export NNN_TMPFILE=/tmp/nnn

# Sync with ~/.bashrc
[ -f ~/.bashrc ] && source "$HOME/.bashrc"

# Automatic login in X server
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi
