" Basics
set nocompatible
syntax on
set number relativenumber
set encoding=utf-8
filetype plugin on
set mouse=a
"set clipboard+=unnamedplus
set noshowmode

" Color Scheme
colorscheme gruvbox
let g:gruvbox_contrast_dark = 'hard'
set termguicolors
set background=dark

" Status Bar
set laststatus=2

" Plugins
call plug#begin('~/.config/nvim-plugins')

Plug 'itchyny/lightline.vim'
Plug 'scrooloose/nerdTree'
Plug 'jiangmiao/auto-pairs'
Plug 'yuttie/comfortable-motion.vim'
Plug 'junegunn/goyo.vim'
Plug 'mboughaba/i3config.vim'
Plug 'kovetskiy/sxhkd-vim'
Plug 'octol/vim-cpp-enhanced-highlight'
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer' }

call plug#end()

" Tab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab


" ~ S o m e  C o o l  S t u f f s ~ "

" Autocompletion
let g:ycm_global_ycm_extra_conf = '/home/matteo/.config/nvim/.ycm_extra_conf.py'
let g:ycm_complete_in_strings = 1
let g:ycm_complete_in_comments = 0

set completeopt-=preview
set shortmess+=c
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" vim-cpp highlights
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_concepts_highlight = 1
let c_no_curly_error=1

" Identation level indicator
set list
let &lcs='tab:| '

" Switch between split more quickly
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Toggle NerdTree
nmap <C-n> :NERDTreeToggle<CR>

" Copy and Paste to/from the system Clipboard
vnoremap <C-c> "+y
map <C-v> "+P

" Highlight the cursoline!
set cursorline

" No auto-commenting new lines pls
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Delete automatically trailing spaces
autocmd BufWritePre * %s/\s\+$//e

" Restart i3 whenever its config file is edited
autocmd BufWritePost ~/.config/i3/config !i3-msg restart

" Run xrdb whenever .Xresources is edited
autocmd BufWritePost ~/.Xresources !xrdb %

" Compile suckless programs whenever their config.h file is edited
"autocmd BufWritePost config.h !make && sudo -A make install

" Compile and run with f5
nnoremap  <special> <buffer> <F5> :w!<Return>:!compiler %<Return>

" CPP
autocmd FileType cpp inoremap <C-o> #include <iostream><Enter><Enter>int main()<Enter>{<Enter>return 0;<Enter>}
